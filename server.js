const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const session = require('express-session');

app.use(cookieParser());

app.use(session({
    secret: '1234',
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: true,
        maxAge: 3600000,
        secure: false
    }
}))

app.get('/', (req, res) => {

    if (!req.session.count) {
        // req.session.count = 1;
        res.redirect('/login');
    } else {
        req.session.count += 1;
        res.redirect('/dashboard');
    }    
     
});

app.get('/create', (req, res) => {
    req.session.count = 1;
    res.send('A new session was created!');
});

app.get('/login', (req, res) => {
    res.send('Login screen')
})

app.get('/dashboard', (req, res) => {
    res.send('Dashboard')
})


app.listen(3000, () => console.log('App started on Port 3000...'));
