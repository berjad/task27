﻿
Task 27 - User Session

Create a basic express with four routes. 
No HTML required. 
Simply use the res.send('some text here');

GET /
Check if a user session exists, if it does, redirect to /dashboard if not, redirect to /login

GET /create
Create a random user session

GET /login
Show the text Login screen

GET /dashboard
Show the text Dashboard